import groupBy from "lodash/groupBy";

function importAll(req) {
  return Object.fromEntries(req.keys().map(key => [key, req(key)]));
}

const cache = importAll(require.context("./images/", true, /.*/));
console.log(cache);

const testImage = str => /.*\/image\d+/.test(str);
const matchGroups = str => {
  const { groups } = str.match(
    /(?<image>image\d+)\/(?<style>style\d+)\/(?<light>light\d+)/
  );
  return groups;
};

const mapLight = ([style, lights]) => {
  return {
    style,
    lights: Object.entries(
      groupBy(lights, ([file, src]) => {
        const groups = matchGroups(file);
        return `${groups.light}`;
      })
    ).map(([light, files]) => {
      const layers = files.map(([file, src]) => ({
        file,
        src
      }));
      return {
        light,
        layers: {
          base: layers.find(({ file }) => /base\./.test(file)),
          mask: layers.find(({ file }) => /mask\./.test(file)),
          light: layers.find(({ file }) => /light\./.test(file))
        }
      };
    })
  };
};

const mapWithGroup = ([image, images]) => {
  return {
    image,
    styles: Object.entries(
      groupBy(images, ([file, src]) => {
        const groups = matchGroups(file);
        return `${groups.style}`;
      })
    ).map(mapLight)
  };
};

const groupImages = arr => {
  return Object.entries(
    groupBy(arr, ([file, src]) => {
      const groups = matchGroups(file);

      return `${groups.image}`;
    })
  ).map(mapWithGroup);
};

const imagesObj = groupImages(
  Object.entries(cache).filter(([file, src]) => testImage(file))
);

export default imagesObj;
// console.log(
//   imagesObj,
//   Object.fromEntries(
//     Object.entries(imagesObj).map(([key, entry]) => {
//       return [
//         key,
//         {
//           base: entry.find(el => /base/.test(el)),
//           mask: entry.find(el => /mask/.test(el)),
//           light: entry.find(el => /light/.test(el))
//         }
//       ];
//     })
//   )
// );
