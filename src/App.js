import React, { useState, useMemo, createContext, useContext } from "react";
import "./App.css";
import styled, { createGlobalStyle, css } from "styled-components";
import { TwitterPicker, SwatchesPicker } from "react-color";
import config from "./config";

const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
    font-family: 'Open Sans', sans-serif;
  }

  button {
    font-weight: 600;
  }

  body {
    margin: 0;
  }
`;

const ImageWrapper = styled.div`
  position: relative;
  margin: 0 auto;
`;

const positionAbove = props => css`
  background-position: top;
  mask-position: top;
  background-size: contain;
  background-repeat: no-repeat;
  mask-size: contain;
  mask-repeat: no-repeat;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
`;
// background: url(${props => props.imageBase});
// background-size: contain;
// background-repeat: no-repeat;
// ${positionAbove}
const BaseImage = styled.img`
  max-width: 100vw;
  max-height: 600px;
`;

const ColorImageOverlay = styled.div`
  background: url(${props => props.imageBase});
  background-color: ${props => props.color};
  background-blend-mode: multiply;
  mask-image: url(${props => props.imageColor});
  ${positionAbove}
`;

const LightImageOverlay = styled.div`
  background: url(${props => props.imageLight});
  mix-blend-mode: screen;
  filter: opacity(1);
  ${positionAbove}
`;

const ControlsContainer = styled.div`
  max-width: 600px;
  margin: 0 auto;
`;

const Room = ({ color, className, imageBase, imageColor, imageLight }) => (
  <ImageWrapper className={className}>
    <BaseImage src={imageBase} />
    <ColorImageOverlay
      color={color}
      imageColor={imageColor}
      imageBase={imageBase}
    />
    <LightImageOverlay imageLight={imageLight} />
  </ImageWrapper>
);

const RoomFade = styled(Room)`
  transition: opacity 1s ease-in-out;
  opacity: ${props => (props.opacity ? "1" : "0")};
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
`;

const ContentSwitch = styled.div`
  position: relative;
  flex: 0 0 auto;
`;

const LayersContext = createContext(null);
const ImageConfigControls = ({ config, children }) => {
  console.log(config);
  const [image, setImage] = useState(null);
  const [style, setStyle] = useState(null);
  const [light, setLight] = useState(null);

  const images = config;
  const currentImage = useMemo(() => {
    if (!image) {
      return images[0];
    }
    return images.find(entry => entry.image === image) || images[0];
  }, [images, image]);

  const styles = currentImage.styles;
  const currentStyle = useMemo(() => {
    if (!style) {
      return styles[0];
    }
    return styles.find(entry => entry.style === style) || styles[0];
  }, [styles, style]);

  const lights = currentStyle.lights;
  const currentLight = useMemo(() => {
    if (!light) {
      return lights[0];
    }
    return lights.find(entry => entry.light === light) || lights[0];
  }, [lights, light]);

  const context = {
    light: currentLight.light,
    setLight,
    image: currentImage.image,
    setImage,
    style: currentStyle.style,
    setStyle,
    images,
    styles,
    lights,
    layers: currentLight.layers
  };

  console.log(context);
  return (
    <LayersContext.Provider value={context}>{children}</LayersContext.Provider>
  );
};

const Content = styled.div`
  display: flex;
  justify-content: center;
`;

const ImageLayers = ({ color, light }) => {
  const context = useContext(LayersContext);
  console.log(context);
  const light1 = context.lights[0];
  const light2 = context.lights[1];

  return (
    <Content>
      <ContentSwitch>
        <Room
          opacity={true}
          color={color}
          imageBase={light1.layers.base.src}
          imageColor={light1.layers.mask.src}
          imageLight={light1.layers.light.src}
        />
        <RoomFade
          opacity={context.light === "light2"}
          color={color}
          imageBase={light2.layers.base.src}
          imageColor={light2.layers.mask.src}
          imageLight={light2.layers.light.src}
        />
      </ContentSwitch>
    </Content>
  );
};

function App() {
  const [color, setColor] = useState("#052f3d");

  const handleChange = (color, event) => {
    setColor(color.hex);
  };

  const colors = [
    "#BDA38E",
    "#876B59",
    "#675C53",
    "#AFA598",
    "#908A7E",
    "#5C626F",
    "#283547"
  ];

  return (
    <>
      <GlobalStyle />
      <ImageConfigControls config={config}>
        <ImageLayers color={color} />

        <ControlsContainer>
          <div
            style={{
              display: "flex",
              justifyContent: "center"
            }}
          >
            <TwitterPicker
              onChange={handleChange}
              color={color}
              colors={colors}
            />
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "center"
            }}
          >
            <SwatchesPicker onChange={handleChange} color={color} />
          </div>
          <Controls />
        </ControlsContainer>
      </ImageConfigControls>
    </>
  );
}

const InnerButton = styled.button`
  background: transparent;
  border: 2px solid #c5c5c5;
  border-radius: 24px;
  color: #7b7b7b;
  height: 100%;
  width: 100%;
  ${({ active }) =>
    active &&
    `
    border-color: #b63dff;
    color: #7e19ba;
  `}
`;

const Button = styled(({ className, ...props }) => {
  return (
    <div className={className}>
      <InnerButton {...props} />
    </div>
  );
})`
  height: 48px;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 8px;
`;

const ButtonRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  ${Button} {
    flex: 1 0 100px;
  }
`;

const Controls = () => {
  const context = useContext(LayersContext);
  return (
    <div>
      {/* <ButtonRow>
        {context.images.map(entry => (
          <Button
            active={entry.image === context.image}
            onClick={() => context.setImage(entry.image)}
            key={entry.image}
          >
            {entry.image}
          </Button>
        ))}
      </ButtonRow> */}
      <ButtonRow>
        {context.styles.map(entry => (
          <Button
            active={entry.style === context.style}
            onClick={() => context.setStyle(entry.style)}
            key={entry.style}
          >
            {entry.style}
          </Button>
        ))}
      </ButtonRow>
      <ButtonRow>
        {context.lights.map(entry => (
          <Button
            active={entry.light === context.light}
            onClick={() => context.setLight(entry.light)}
            key={entry.light}
          >
            {entry.light}
          </Button>
        ))}
      </ButtonRow>
    </div>
  );
};

export default App;
